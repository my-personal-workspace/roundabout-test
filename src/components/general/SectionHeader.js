import React, {useState} from 'react';
import Grid from '@mui/material/Grid';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import '../../assets/scss/components/sectionHeader.scss';

const Bars = ({ title, list }) =>  {

    const [value, setValue] = useState(list.filter(el => el.default)[0].days);

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    return (
        <Grid container className="titleContainer">
            <Grid item xs={8}>
            <h3 style={{textAlign: 'left'}}>{title}</h3>
            </Grid>
            <Grid item xs={4} style={{textAlign: 'right'}}>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 80 }}>
                <Select
                value={value}
                className="selectInput"
                onChange={handleChange}
                autoWidth
                >
                {list.map(el => <MenuItem key={el.name} value={el.days}>{el.name}</MenuItem>)}
                </Select>
            </FormControl>
            </Grid>
        </Grid>
    )
};

export default Bars;
