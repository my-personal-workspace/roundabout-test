import React from 'react';
import '../../assets/scss/components/charts.scss';

const Bars = ({ data }) =>  {
    const yData = () => {
        const temp = [];
        const singleDiv = (data.max - data.min) / 5;
        let currentCount = data.max
        while (currentCount >= data.min) {
            temp.push(currentCount);
            currentCount -= singleDiv
        }
        return temp
    }

    const xData = () => {
        return data.bars
    }

    return (
        <div className="barContainer">
            <div className="legend">
                <span>Project {data.projectX}</span>
                <span>Project {data.projectY}</span>
            </div>
            <div className="container">
                <div className="yData">
                    {yData().map(el => <div key={el}>{el}m</div>)}
                </div>
                <div className="xData">
                    {xData().map((el) => {
                        return (
                        <div key={el.label}>
                            <div className="placeholder" style={{height: `calc(${el[data.projectX] > el[data.projectY] ? el[data.projectX] : el[data.projectY]}% + 20px)`}}>
                                <div className="hoverField">
                                    <span>Project {data.projectX} = {el[data.projectX]} %</span>
                                    <span>Project {data.projectY} = {el[data.projectY]} %</span>
                                </div>
                            </div>
                            <div className="bars">
                                <div style={{height: `${el[data.projectX]}%`}}>
                                </div>
                                <div style={{height: `${el[data.projectY]}%`}}>
                                </div>
                            </div>
                            <div className="label">
                                {el.label}
                            </div>
                        </div>
                    )})}
                </div>
            </div>
        </div>

    )
};

export default Bars;
