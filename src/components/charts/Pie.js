import React, {useState} from 'react';

import { PieChart } from 'react-minimal-pie-chart';

import '../../assets/scss/components/charts.scss';

const Pie = ({ data }) =>  {
    const [selected, setSelected] = useState(undefined);
    const segmentsStyle = { transition: 'stroke .3s', cursor: 'pointer' };

    return (
        <div className="barContainer pie">
            <div className="chartContainer">
                <PieChart
                    lineWidth={40}
                    radius={45}
                    segmentsStyle={(index) => {
                        return index === selected
                            ? { ...segmentsStyle, strokeWidth: 25 }
                            : segmentsStyle;
                        }}
                    onMouseOver={(_, index) => {
                        setSelected(index);
                    }}
                    data={data}
                />
                <span>{data.reduce((a, b) => a + (b.hours || 0), 0)} Hr</span>
            </div>
            <div className="verticalLegend">
                {data.map((el) => {
                    return (
                        <div key={el.title} style={{background: el.color}}>
                            <div className="icon">
                                {React.createElement(el.icon, {
                                    key: el.title,
                                    el: el
                                    })}
                            </div>
                            <div className="text">
                                <h5>{el.title}</h5>
                                <span>{el.hours} Hour</span>
                            </div>
                        </div>
                    )
                })

                }
            </div>

        </div>
    )
}

export default Pie;