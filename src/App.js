import * as React from 'react';
import { styled } from '@mui/material/styles';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import MuiDrawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Grid from '@mui/material/Grid';
import ListItem from '@mui/material/ListItem';
import DashboardIcon from '@mui/icons-material/Dashboard';
import AvTimerIcon from '@mui/icons-material/AvTimer';
import AccessTimeFilledIcon from '@mui/icons-material/AccessTimeFilled';
import ContentPasteIcon from '@mui/icons-material/ContentPaste';
import SupportIcon from '@mui/icons-material/Support';
import DateRangeIcon from '@mui/icons-material/DateRange';
import SettingsIcon from '@mui/icons-material/Settings';
import Bars from './components/charts/Bars'
import Pie from './components/charts/Pie'
import SectionHeader from './components/general/SectionHeader'
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import BusinessCenterIcon from '@mui/icons-material/BusinessCenter';
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';

import './assets/scss/app.scss';


const darkTheme = createTheme({
  palette: {
    text: {
      primary: '#201f24',
      secondary: '#97989f',
      green: '#e2fa5c',
      red: '#efb7a6',
      blue: '#b7edef'
    },
    background: {
      default: '#21222d',
      white: '#ffffff',
      grey: '#fafafa'
    }
  },
});

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme }) => ({
    flexShrink: 0,
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} - 16px)`,
    [theme.breakpoints.up('sm')]: {
      width: `calc(${theme.spacing(9)} - 16px)`,
    },
    '& .MuiDrawer-paper': ({
      overflowX: 'hidden',
      width: `calc(${theme.spacing(7)} - 16px)`,
    }),
  }),
)

const data = {
  min: 0,
  max: 50,
  projectX: 'X',
  projectY: 'Y',
  bars: [
    {
      label: 'Sun',
      X: 15,
      Y: 60
    },
    {
      label: 'Mon',
      X: 15,
      Y: 60
    },
    {
      label: 'Tue',
      X: 15,
      Y: 60
    },
    {
      label: 'Wed',
      X: 15,
      Y: 100
    },
    {
      label: 'Thu',
      X: 15,
      Y: 60
    },
    {
      label: 'Fri',
      X: 15,
      Y: 60
    }
  ]
}

const barsList = [
  {
    name: 'Last 3 days',
    days: 3
  },
  {
    name: 'Last 6 days',
    days: 6,
    default: true
  },
  {
    name: 'Last 14 days',
    days: 14
  },
  {
    name: 'Last 28 days',
    days: 28
  }
]

const reportList = [
  {
    name: 'Today',
    days: 0,
  },
  {
    name: 'Weekly',
    days: 7,
    default: true
  },
  {
    name: 'Monthly',
    days: 32
  },
  {
    name: 'Last 28 days',
    days: 28
  }
]

const timeList = [
  {
    name: 'Today',
    days: 0,
    default: true
  },
  {
    name: 'Yesterday',
    days: 1
  }
]

const pieData = [
  { title: 'Branding', value: 40, icon: LocalOfferIcon, hours: 7, color: darkTheme.palette.text.red },
  { title: 'Office', value: 30, icon: BusinessCenterIcon, hours: 3, color: darkTheme.palette.text.blue },
  { title: 'Transport', value: 30, icon: DirectionsCarIcon, hours: 4, color: darkTheme.palette.text.green },
]

function App() {
  return (
    <ThemeProvider theme={darkTheme}>
    <div className="App">
       <Drawer variant="permanent" className="drawer">
        <List style={{marginBottom: '20px'}}>
          <ListItem style={{paddingLeft: 0}}>
            <AvTimerIcon style={{color: darkTheme.palette.text.red, fontSize: '40px'}}/>
          </ListItem>
        </List>
        <List className="listItems">
          <ListItem className="selectedItem">
            <DashboardIcon/>
          </ListItem>
          <ListItem>
            <AccessTimeFilledIcon />
          </ListItem>
          <ListItem>
            <ContentPasteIcon />
          </ListItem>
          <ListItem>
            <SupportIcon />
          </ListItem>
          <ListItem>
            <DateRangeIcon />
          </ListItem>
          <ListItem>
            <SettingsIcon />
          </ListItem>
        </List>
      </Drawer>
      <Grid container spacing={8} style={{padding: '40px'}} >
        <Grid item xs>
          <SectionHeader title="Time Spend on projects" list={barsList}/>
          <Bars data={data} />
          <SectionHeader title="Time tracker" list={timeList}/>
        </Grid>
        <Grid item xs={3}>
          <div style={{background: darkTheme.palette.text.red, height: '150px'}}></div>
          <SectionHeader title="Report" list={reportList}/>
          <Pie data={pieData}/>
        </Grid>
      </Grid>
    </div>
      </ThemeProvider>
  );
}

export default App;
